const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema({
   f_name: {
       type: String,
       required: true,
       minlength: 2,
       maxlength: 20
   },
   l_name: {
       type: String,
       required: true,
       minlength: 2,
       maxlength: 40
   },
    birthday: String,
    group: {
       type: String,
        required: true
    },
    username: {
       type: String,
        required: true,
        index: { unique: true }
    },
    password: {
       type: String,
        required: true
    },
    role: {
       type: String,
        required: true
    },
    last_auth_date: {
        type: Date
    }
});

module.exports = mongoose.model("User", User);
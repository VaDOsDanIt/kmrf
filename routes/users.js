const express = require('express');
const mongoose = require('mongoose');
const User = require("../models/user");
const router = express.Router();

router.get('/test', (req, res) => {
    const newUser = new User({f_name: "Vadim", l_name: "Tartakovsky"});
    newUser.save();

});

module.exports = router;

import React from 'react';
import AuthPage from "./pages/authPage/AuthPage";

function App() {
  return (
    <div className="App">
      <AuthPage/>
    </div>
  );
}

export default App;

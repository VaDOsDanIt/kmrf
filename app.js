const express = require('express');
const config = require('config');
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const User = require("./models/user");
const app = express();

const usersApi = require('./routes/users');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api/users', usersApi);

mongoose.connect(config.get("mongoURI"), { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, () => {
    try {
        console.log("Database is connected.");
    } catch (e) {
        console.log(e.message);
    }
});

app.listen(config.get("PORT"), () => {
    try {
        console.log("Server started on: ", config.get("PORT"));
    } catch (e) {
        console.log(e.message);
    }
});